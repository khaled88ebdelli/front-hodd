import React, { useEffect } from "react";
import { Layout } from 'antd';
import Sidebar from "./Sidebar";
import Header from "./Header";
import { useSelector } from "react-redux";
import { selectLoginState } from "../../features/login/LoginSlice";
import { navigate } from "hookrouter";
import { useState } from "react";

const { Content } = Layout;

const LayoutIndex = ({ children }) => {
  const { isLogged } = useSelector(selectLoginState)
  const [collapsed, setCollapsed] = useState(false)
  useEffect(() => {
    if (!isLogged) {
      navigate('/login', true);
    }
  }, [isLogged])
  return (
    <Layout>
      <Sidebar collapsed={collapsed} setCollapsed={setCollapsed}/>
      <Layout>
        <Header toggle={() => setCollapsed(c => !c)} collapsed={collapsed} />
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>{children}</Content>
      </Layout>
    </Layout>
  );
}
export default LayoutIndex;
