import React from "react";
import { Layout, Menu, Divider } from "antd";
import { TeamOutlined, VideoCameraOutlined } from '@ant-design/icons';
import { isMobile } from 'react-device-detect';
import ProfileCard from '../../shared/ProfileCard'
import "./index.scss";
import { A } from "hookrouter";

const { Sider } = Layout;
const { SubMenu } = Menu;

const Sidebar = ({collapsed, setCollapsed}) => {
  return (
    <Sider 
    className="layout-sider"
    trigger={null} 
    collapsible
    collapsedWidth={isMobile ? 0 : 80}
    breakpoint="lg"
    onBreakpoint={broken => {
      setCollapsed(true)
      console.log(broken);
    }}
    collapsed={collapsed}>
      <A href='/profile'><ProfileCard collapsed={collapsed} /></A>
      <Divider style={{ width: "90%", textAlign: 'center' }} />
      <Menu
        mode="inline"
        defaultSelectedKeys={[""]}
        defaultOpenKeys={[""]}
      >
        <SubMenu
          key="dashboard"
          title={
            <span>
              <VideoCameraOutlined />
              <span>Videos Management</span>
            </span>
          }
        >
          <Menu.Item key="videos"><A href="/videos">Videos</A></Menu.Item>
          <Menu.Item key="add_video"><A href="/videos/add">Create Video</A></Menu.Item>
          <Menu.Item key="claims">Claims</Menu.Item>
        </SubMenu>
        <SubMenu
          key="teamManagement"
          title={
            <span>
              <TeamOutlined />
              <span>Team Management</span>
            </span>
          }
        >
          <Menu.Item key="9">Profile Management</Menu.Item>
          <Menu.Item key="10">Team Management</Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};
export default Sidebar;
