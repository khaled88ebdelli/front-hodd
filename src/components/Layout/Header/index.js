import React from "react";
import { Layout, Input } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import Notifications from "../../Notifications";
import "./index.scss";

const { Header } = Layout;
const { Search } = Input;

const MainHeader = ({ toggle, collapsed }) => {
  return (
    <Header className="app-header">
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
      <Search
        placeholder="Recherche..."
        onSearch={value => console.log(value)}
      />
      <Notifications />
    </Header>
  );
};

export default MainHeader;
