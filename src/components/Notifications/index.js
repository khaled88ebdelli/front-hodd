import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Menu, Badge } from "antd";
import { isMobile } from 'react-device-detect';
import { LogoutOutlined, SettingOutlined, MessageOutlined } from '@ant-design/icons';
import { signout, selectLoginState } from "../../features/login/LoginSlice";
import { clearMessage } from "../../features/internet/internetSlice";
import { navigate } from "hookrouter";
import "./index.scss";
import { selectChatbox, showChatbox } from "../../features/chatbox/chatboxSlice";

const { SubMenu } = Menu;

const Notification = () => {
  const [current, setCurrent] = useState('')
  const { isLogged } = useSelector(selectLoginState)
  const { newMessages } = useSelector(selectChatbox)
  const dispatch = useDispatch()
  const handleClick = (e) => setCurrent(e.key)
  const handleChatbox = (e) => dispatch(showChatbox())
  const handleLogout = () => {
    if (isLogged) {
      dispatch(clearMessage())
      dispatch(signout())
    }
    navigate('/login');
  }
  return (
    <Menu className="app-notifications-group" onClick={handleClick} selectedKeys={[current]} mode="horizontal">
      <Menu.Item key="logout" onClick={handleLogout} >
        <LogoutOutlined />
       {!isMobile && <span>Logout</span>}
        </Menu.Item>
      <Menu.Item key="chatbox" onClick={handleChatbox} >
        <Badge count={newMessages} overflowCount={10}><MessageOutlined /></Badge>
        {!isMobile && <span>Chatbox</span>}
      </Menu.Item>
      <SubMenu title={<span className="submenu-title-wrapper">
        <SettingOutlined />
        {!isMobile &&  'Settings' }
      </span>} >
        <Menu.ItemGroup title="Item 1">
          <Menu.Item key="setting:1">Option 1</Menu.Item>
          <Menu.Item key="setting:2">Option 2</Menu.Item>
        </Menu.ItemGroup>
      </SubMenu>
    </Menu>
  );
};

export default Notification;
