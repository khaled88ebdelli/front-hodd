import React from 'react'
import { Descriptions, Avatar } from 'antd'
import moment from 'moment';

function UserInfo(props) {
    const { firstname, lastname, email, birthdate, provider, picture } = props
    return (
        <Descriptions title={<Avatar shape="square" src={picture} size="large" />} layout="horizontal">
            <Descriptions.Item label="First Name">{firstname}</Descriptions.Item>
            <Descriptions.Item label="Last Name">{lastname}</Descriptions.Item>
            <Descriptions.Item label="Email">{email}</Descriptions.Item>
            <Descriptions.Item label="Birthdate">{moment(birthdate).format("DD-MM-YYYY")}</Descriptions.Item>
            <Descriptions.Item label="Provider">{provider}</Descriptions.Item>
        </Descriptions>
    )
}

export default UserInfo
