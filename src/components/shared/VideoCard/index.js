import React from 'react'
import { Card } from 'antd';
import Description from '../VideoDescription';
import './style.scss'

const { Meta } = Card;


function VideoCard({ thumbnail, title, createdAt, user, loading, openVideoModal }) {
    const URL = window.URL || window.webkitURL;
    const blob = new Blob([new Uint8Array(thumbnail.data)], { type: 'image/jpeg' });
    const image = URL.createObjectURL(blob)
    return (
        <Card
            hoverable
            loading={loading}
            style={{ margin: '0 10px 22px' }}
            cover={<div className="img-hover-zoom img-hover-zoom--blur">
                <img onClick={openVideoModal} width="100%" alt="video screen shot" src={image} />
            </div>} >
            <Meta title={title} description={<Description user={user} createdAt={createdAt} />} />
        </Card>
    )
}

export default VideoCard
