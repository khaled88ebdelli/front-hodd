import React from 'react'
import { Tag } from 'antd'
import {
   TwitterOutlined,
   GoogleOutlined,
   FacebookOutlined,
   UserOutlined,
   CheckCircleTwoTone,
   SmileOutlined
} from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectInternet } from '../../features/internet/internetSlice';

function ProviderTag({ provider, collapsed, ...rest }) {
   const { ws } = useSelector(selectInternet)

   const WsConnectionTag = (props) => (
      ws.opened ? <CheckCircleTwoTone {...props} twoToneColor="#52c41a" /> :
         <SmileOutlined {...props} rotate={180} twoToneColor="#FF0000" />)

   const providerSuffix = collapsed ? <WsConnectionTag style={{margin: '0.5px'}} /> : <> Connected <WsConnectionTag /></>

   switch (provider) {
      case "google":
         return <Tag {...rest} color="#3b5999"><GoogleOutlined /> Google {providerSuffix} </Tag>

      case "facebook":
         return <Tag {...rest} color="#3b5999"><FacebookOutlined /> Facebook {providerSuffix} </Tag>

      case "twitter":
         return <Tag {...rest} color="#55acee"><TwitterOutlined /> Twitter {providerSuffix} </Tag>

      default:
         return <Tag {...rest} color="#55acee"><UserOutlined /> Local {providerSuffix} </Tag>
   }
}

export default ProviderTag
