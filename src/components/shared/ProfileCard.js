import React from 'react'
import { useSelector } from 'react-redux'
import { selectLoginState } from '../../features/login/LoginSlice'
import ProviderTag from '../shared/ProviderTag'

function ProfileCard({collapsed}) {
    const { provider, picture } = useSelector(selectLoginState)
    return (
        <span>
            <div style={{display: 'block', margin: '10px'}}>
            <img 
            src={picture || 'no-picture-yet.jpg'} 
            width="100%"
            height="80px"
            alt="logo" />
            <ProviderTag collapsed={collapsed} style={{width: '100%', padding: '0 5px', fontSize: collapsed ? '9px' : '14px'}} provider={provider}/>
            </div>
        </span>
    )
}

export default ProfileCard
