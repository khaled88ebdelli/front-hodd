import React from 'react'
import { Space } from 'antd'
import Description from './VideoDescription';

function VideoAddNoification({ video }) {
    const URL = window.URL || window.webkitURL;
    const blob = new Blob([new Uint8Array(video.thumbnail.data)], { type: 'image/jpeg' });
    const image = URL.createObjectURL(blob)
    console.log('video.user', video.user);
    return (
        <Space>
            <img src={image} alt="new video added" width="100px" />
            <Description user={video.user} createdAt={video.createdAt} />
        </Space>
    )
}

export default VideoAddNoification
