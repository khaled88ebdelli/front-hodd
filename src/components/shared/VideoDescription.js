import React from 'react'
import { Space, Avatar, Badge } from 'antd'
import { CalendarOutlined } from '@ant-design/icons'
import moment from 'moment'


const Description = ({ user, createdAt }) => (
    <Space>
        <Avatar style={{ margin: '-15px 10px 0 0' }} src={user.picture} />
        <Badge >
            {user.firstname} {user.lastname}
            <p style={{ marginTop: '5px', fontSize: '12px' }}> <CalendarOutlined /> {moment(createdAt).format('DD/MM/YYYY @ H:mm:ss')}</p>
        </Badge>
    </Space>
)

export default Description