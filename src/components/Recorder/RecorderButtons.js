import React from 'react'
import { Space, Button, Radio } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import { isMobile } from 'react-device-detect';
import { selectVideos, loadMediaBlobUrl, joinLive, leaveLive } from '../../features/videos/VideosSlice'
import { VideoCameraAddOutlined, VideoCameraOutlined, FundProjectionScreenOutlined, CameraOutlined, DeleteOutlined, SaveOutlined, PlayCircleFilled, StopFilled } from '@ant-design/icons'
import Text from 'antd/lib/typography/Text'

function RecorderButtons({ startRecording, stopRecording, screenRecording, CamRecording, saveRecordedVideo }) {
    const { recording, mediaBlobUrl, live } = useSelector(selectVideos)
    const dispatch = useDispatch()
    return (
        <Space>
            {!recording && !mediaBlobUrl && <Button icon={<VideoCameraAddOutlined />} type="ghost" onClick={startRecording}>
                {!isMobile && <Text strong>Start Recording</Text>}
            </Button>}
            {!recording && mediaBlobUrl && <Button icon={<DeleteOutlined />} type="danger" onClick={() => dispatch(loadMediaBlobUrl(null))}>
                {!isMobile && <Text strong>Revoke Recorded Video</Text>}
            </Button>}
            {!recording && mediaBlobUrl && <Button icon={<SaveOutlined />} type="primary" onClick={saveRecordedVideo}>
                {!isMobile && <Text strong>Save Video</Text>}
            </Button>}
            {recording && <Button className="recording" icon={<VideoCameraOutlined />} type="danger" onClick={stopRecording}>
                {!isMobile && <Text strong>Stop Recording</Text>}
            </Button>}
            {!recording && <Button icon={!live ? <PlayCircleFilled/> : <StopFilled />}  className={live ? 'recording' : ''}
            onClick={() => !live ? dispatch(joinLive()) : dispatch(leaveLive())}>
                {!isMobile && <Text strong>{!live ? 'Join Live' : 'Leave Live'}</Text>}
            </Button>}

            {!live && !recording && !mediaBlobUrl && <Radio.Group defaultValue="webcam" buttonStyle="solid">
                <Radio.Button value="screen" onClick={screenRecording}>
                    <Space>
                        <FundProjectionScreenOutlined /> {!isMobile && <Text strong>Screen Mode</Text>}
                    </Space>
                </Radio.Button>
                <Radio.Button value="webcam" onClick={CamRecording}>
                    <Space>
                        <CameraOutlined /> {!isMobile && <Text strong>Cam Mode</Text>}
                    </Space>
                </Radio.Button>
            </Radio.Group>}
        </Space>
    )
}

export default RecorderButtons
