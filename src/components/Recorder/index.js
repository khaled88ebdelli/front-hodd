import React, { useRef, useEffect, useCallback, useState } from 'react'
// import { isMobile } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux'
import RecorderButtons from './RecorderButtons'
import { loadMediaBlobUrl, recordingStart, recordingStop, selectVideos, newStreamSent } from '../../features/videos/VideosSlice'
import './style.scss'

const hasUserMedia = () => {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)
}
const isMediaActive = (mediaStream) => {
    return !!(mediaStream && mediaStream.active)
}
const Recorder = ({ mimtype = 'video/webm;codecs=vp8,opus', audio = true, video = true, screen = false, onSubmit }) => {
    const { recording, mediaBlobUrl, streamReceived, live } = useSelector(selectVideos)
    const [screenMode, setScreenMode] = useState(screen)
    const URL = window.URL || window.webkitURL;
    const canvasRef = useRef()
    const imageRef = useRef()
    const dispatch = useDispatch()
    const videoRef = useRef()
    const streamVideo = useRef()
    const mediaRecorder = useRef()
    const chunks = useRef([])

    const getDevice = useCallback(() => {
        if (hasUserMedia()) {
            if (!screenMode) {
                return navigator.mediaDevices.getUserMedia({ audio, video, mimtype })
            }
            return navigator.mediaDevices.getDisplayMedia({ audio, video, mimtype })
        }
    }, [screenMode, audio, video, mimtype])

    useEffect(() => {
        const loadMediaStream = async () => {
            if (!recording && !live) {
                streamVideo.current = await getDevice();
                if (videoRef.current && isMediaActive(streamVideo.current)) {
                    videoRef.current.srcObject = streamVideo.current
                }
            }
            if (recording && streamVideo.current) {
                if (videoRef.current && isMediaActive(streamVideo.current)) {
                    videoRef.current.srcObject = streamVideo.current
                }
            }
        }
        const closeMediaStream = async () => {
            if (!recording && streamVideo.current) {
                await streamVideo.current.getTracks().forEach(track => track.stop())
            }
        }
        loadMediaStream()
        return () => closeMediaStream()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getDevice, live])

    const onStopRecording = useCallback((interval) => {
        clearInterval(interval)
        const blobURL = URL.createObjectURL(new Blob([...chunks.current], { type: mimtype }));
        dispatch(loadMediaBlobUrl(blobURL))
    }, [dispatch, mimtype, URL])

    useEffect(() => {
        if (streamReceived && imageRef.current) {
            imageRef.current.src = streamReceived
        }
        if (mediaBlobUrl && videoRef.current) {
            videoRef.current.src = mediaBlobUrl
        }
    }, [streamReceived, mediaBlobUrl])

    const startLiveRecordingHandler = async () => {
        if (streamVideo.current && videoRef.current && canvasRef.current) {
            videoRef.current.srcObject = streamVideo.current
            dispatch(loadMediaBlobUrl(null))
            mediaRecorder.current = new MediaRecorder(streamVideo.current, { mimeType: mimtype });
            mediaRecorder.current.ondataavailable = (e) => chunks.current.push(e.data)
            const width = streamVideo.current.getVideoTracks()[0].getSettings().width;
            const height = streamVideo.current.getVideoTracks()[0].getSettings().height;
            canvasRef.current.width = width;
            canvasRef.current.height = height;
            const interval = setInterval(() => {
                if (canvasRef.current) {
                    canvasRef.current.getContext('2d').drawImage(videoRef.current, 0, 0, height, width);
                    const data = canvasRef.current.toDataURL('image/jpeg');
                    dispatch(newStreamSent(data))
                }
            }, 40)
            mediaRecorder.current.onstop = () => onStopRecording(interval)
            mediaRecorder.current.start()
            dispatch(recordingStart())
        }
    };

    const stopLiveRecordingHandler = () => {
        if (mediaRecorder.current) {
            mediaRecorder.current.stop()
            dispatch(recordingStop())
        }
    }
    return (
        <>
            <RecorderButtons
                screenRecording={() => setScreenMode(true)}
                CamRecording={() => setScreenMode(false)}
                startRecording={startLiveRecordingHandler}
                stopRecording={stopLiveRecordingHandler}
                saveRecordedVideo={onSubmit}
            />
            {!live && <div className="liveVideoPlayer" >
                <video ref={videoRef} id="video" autoPlay controls={recording} />
            </div>}
            {live && <div className="liveVideoPlayer">
                <img ref={imageRef} alt='live' />
            </div>}
            <canvas ref={canvasRef} hidden />
        </>
    )
}

export default Recorder;