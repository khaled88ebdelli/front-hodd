import React, { useEffect, useState } from 'react'
import OAuth from '../helpers/OAuth'
import { Spin } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { selectInternet } from '../features/internet/internetSlice';
import { usePopup } from '../hooks/usePopup';
import { loginSuccess, popupClose } from '../features/login/LoginSlice';
import { navigate } from 'hookrouter';

const providers = ['twitter', 'google', 'facebook']

const SocialAuth = ({ auth }) => {

  const [loading, setLoading] = useState(true)
  const [provider, setProvider] = useState()
  const { ws } = useSelector(selectInternet)
  const [popup, openPopup] = usePopup()
  const dispatch = useDispatch()

  useEffect(() => {
    console.log(`${ws.message.event} message`, ws.message.payload);
    if (ws.message.event !== '') {
      if (popup.current && !popup.current.closed) {
        setTimeout(() => popup.current.close())
      }
      if (popup.current && popup.current.closed) {
        dispatch(popupClose())
      }
      dispatch(loginSuccess(ws.message.payload))
      navigate('/', true);
    }
  }, [ws.message, popup, dispatch])

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/wake-up`).then(res => {
      if (res.ok) {
        setLoading(false)
      }
    })
  }, [ws.opened])

  const startAuth = (prv) => {
    if (ws.opened && !popup.current) {
      setProvider(prv)
      popup.current = openPopup(prv)
    } else if (ws.opened && popup.current) {
      if (prv === provider) {
        popup.current.focus()
      } else {
        popup.current.close()
        popup.current = openPopup(prv)
      }
    }
  }

  const buttons = (providers) => providers.map(provider =>
    <OAuth
      auth={auth}
      provider={provider}
      startAuth={() => startAuth(provider)}
      key={provider}
    />
  )
  return (
    <div>
      {loading
        ? <Spin />
        : buttons(providers)
      }
    </div>
  )
}
export default SocialAuth