import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import logger from 'redux-logger'
import loginReducer from '../features/login/LoginSlice';
import registerReducer from '../features/register/RegisterSlice';
import internetReducer from '../features/internet/internetSlice';
import profileReducer from '../features/profile/ProfileSlice';
import chatboxReducer from '../features/chatbox/chatboxSlice';
import videosReducer from '../features/videos/VideosSlice';

export default configureStore({
  reducer: {
    login: loginReducer,
    register: registerReducer,
    internet: internetReducer,
    profile: profileReducer,
    chatbox: chatboxReducer,
    videos: videosReducer,
  },
  middleware: [...getDefaultMiddleware({
    serializableCheck: false,
  }), logger]
});
