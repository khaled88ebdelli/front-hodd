import { expose } from "comlink";
const fetchBlob = async (mediaBlobUrl) => {
    // First, we'll fetch the media file
    const response = await fetch(mediaBlobUrl)
    // Once the file has been fetched, we'll convert it to a `Blob`
    const blob = await response.blob()
    return blob
};

expose(fetchBlob);
