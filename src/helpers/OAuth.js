/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'

const OAuth = ({provider, auth, startAuth}) => {
  const classProvider = provider + '-connect'
  return (
    <div>
      <a href="#" className="social-button" id={classProvider}
        onClick={startAuth}>
        <span>{auth} with {provider.charAt(0).toUpperCase() + provider.substr(1)}</span>
      </a>
    </div>
  )
}

export default OAuth;