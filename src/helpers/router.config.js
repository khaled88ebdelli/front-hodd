import React from 'react'
import { Login } from "../features/login/Login";
import { Register } from "../features/register/Register";
import HomePage from '../pages/HomePage';
import ProfilePage from '../pages/ProfilePage';
import VideosPage from '../pages/VideosPage';

export const publicRoutes = {
  '/login': () => <Login />,
  '/register': () => <Register />
}
export const privateRoutes = {
  '/': () => <HomePage />,
  '/profile': () => <ProfilePage />,
  '/videos': () => <VideosPage />,
}
