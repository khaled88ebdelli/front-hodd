export const extractFramesFromVideo = (videoDataUrl, currentTime = 2)  => {
    return new Promise(async (resolve) => {
      let video = document.createElement("video");
      let seekResolve;
      video.addEventListener('seeked', async function () {
        if (seekResolve) seekResolve();
      });
  
      video.src = videoDataUrl;
  
      // workaround chromium metadata bug (https://stackoverflow.com/q/38062864/993683)
      while ((video.duration === Infinity || isNaN(video.duration)) && video.readyState < 2) {
        await new Promise(r => setTimeout(r, 1000));
        video.currentTime = 10000000 * Math.random();
      }
  
      let canvas = document.createElement('canvas');
      let context = canvas.getContext('2d');
      let [w, h] = [video.videoWidth, video.videoHeight]
      canvas.width = w;
      canvas.height = h;
  
      let base64ImageDataOriginal = null;
      let base64ImageDataResized = null;
  
      video.currentTime = currentTime;
      await new Promise(r => seekResolve = r);
      context.drawImage(video, 0, 0, w, h);
      base64ImageDataOriginal = canvas.toDataURL("image/jpeg", 0.8);
      context.drawImage(video, 0, 0, '320', '240');
      base64ImageDataResized = canvas.toDataURL("image/jpeg", 0.6);
      resolve([base64ImageDataOriginal, base64ImageDataResized]);
    });
  };