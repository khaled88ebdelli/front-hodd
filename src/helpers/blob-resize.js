export function processfile(blob) {
    return new Promise((resolve, reject) => {
         // read the files
    var reader = new FileReader();
    reader.readAsArrayBuffer(blob);

    reader.onload = function (event) {
        // blob stuff
        var blob = new Blob([event.target.result]); // create blob...
        window.URL = window.URL || window.webkitURL;
        var blobURL = window.URL.createObjectURL(blob); // and get it's URL

        // helper Image object
        var image = new Image();
        image.src = blobURL;
        image.onload = function () {
            // have to wait till it's loaded
        const resized = resizeMe(image); // resized image url
        // reader.removeEventListener('onload', event)
        resolve(resized)
        }
    };
    })
    
   
}

// === RESIZE ====

function resizeMe(img, max_width = 300, max_height = 250) {

    var canvas = document.createElement('canvas');

    var width = img.width;
    var height = img.height;

    // calculate the width and height, constraining the proportions
    if (width > height) {
        if (width > max_width) {
            //height *= max_width / width;
            height = Math.round(height *= max_width / width);
            width = max_width;
        }
    } else {
        if (height > max_height) {
            //width *= max_height / height;
            width = Math.round(width *= max_height / height);
            height = max_height;
        }
    }
    // resize the canvas and draw the image data into it
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    return canvas.toDataURL("image/jpeg", 0.5); // get the data from canvas as 70% JPG (can be also PNG, etc.)

    // you can get BLOB too by using canvas.toBlob(blob => {});

}