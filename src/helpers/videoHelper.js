export const constraints = {
    qvgaConstraints: {
        video: { width: { exact: 320 }, height: { exact: 240 } }
    },

    vgaConstraints: {
        video: { width: { exact: 640 }, height: { exact: 480 } }
    },

    hdConstraints: {
        video: { width: { exact: 1280 }, height: { exact: 720 } }
    },

    fullHdConstraints: {
        video: { width: { exact: 1920 }, height: { exact: 1080 } }
    }
}

export function getVideoDimensionsOf(url){
	return new Promise(function(resolve){
		// create the video element
		let video = document.createElement('video');

		// place a listener on it
		video.addEventListener( "loadedmetadata", function () {
			// retrieve dimensions
			let height = this.videoHeight;
			let width = this.videoWidth;
			// send back result
			resolve({
				height : height,
				width : width
			});
		}, false );

		// start download meta-datas
		video.src = url;
	});
}