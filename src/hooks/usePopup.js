import { useRef } from "react"
import { useSelector } from "react-redux";
import { selectInternet } from "../features/internet/internetSlice";

export const usePopup = () => {
    const {ws} = useSelector(selectInternet)
    const popupRef = useRef()
    const openPopup = (provider) => {
        const width = 600, height = 600
        const left = (window.innerWidth / 2) - (width / 2)
        const top = (window.innerHeight / 2) - (height / 2)
        const url = `${process.env.REACT_APP_API_URL}/${provider}?socketId=${ws.id}`;
        popupRef.current = window
        return popupRef.current.open(url, '',
            `toolbar=no, location=no, directories=no, status=no, menubar=no, 
          scrollbars=no, resizable=no, copyhistory=no, width=${width}, 
          height=${height}, top=${top}, left=${left}`
        )
    }
    return [popupRef, openPopup]
}