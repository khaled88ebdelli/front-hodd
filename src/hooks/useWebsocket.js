/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { useRef, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectInternet, wsOnMessage, wsOpened, wsClosed } from '../features/internet/internetSlice'
import { selectVideos, newStreamReceived, fetchVideosAsync } from '../features/videos/VideosSlice'
import io from 'socket.io-client'
import { receiveMessage } from '../features/chatbox/chatboxSlice'
import { notification } from 'antd'
import VideoAddNoification from '../components/shared/VideoAddNoification'

const wsUrl = process.env.REACT_APP_API_URL

const useWebsocket = ({ isOnline }) => {
    const { ws } = useSelector(selectInternet)
    const { streamSent, live } = useSelector(selectVideos)
    const dispatch = useDispatch()
    const wsRef = useRef()
    const onOauthMessage = (msg) => {
        dispatch(wsOnMessage(msg))
    }
    const onOpen = () => {
        // console.log('WS client opened', wsRef.current.id)
        dispatch(wsOpened(wsRef.current.id))
    }
    const onNewVideo = (video) => {
        if (video) {
            notification.open({
                message: 'New Video Added',
                description: <VideoAddNoification video={video} />
            });
            dispatch(fetchVideosAsync())
        }
    }
    const onChatmessage = (msg) => {
        console.log('received message', msg)
        dispatch(receiveMessage(msg))
    }
    const onStreaming = (stream) => {
        console.log('WS client stream', stream)
        dispatch(newStreamReceived(stream))
    }
    const onClose = () => {
        console.log('WS client closed')
        dispatch(wsClosed())
    }
    const onError = (e) => {
        console.log('WS client errored', e)
        dispatch(wsClosed())
    }
    const onConnectError = (e) => {
        console.log('WS client connect errored', e)
        dispatch(wsClosed())
    }
    const initWebsocket = useCallback(() => {
        if (isOnline && (!ws.opened && !wsRef.current)) {
            wsRef.current = io(wsUrl, { reconnectionAttempts: 10, reconnectionDelay: 3000 })
            wsRef.current.on('oauth', onOauthMessage)
            wsRef.current.on('connect', onOpen)
            wsRef.current.on('new video', onNewVideo)
            wsRef.current.on('chat message', onChatmessage)
            wsRef.current.on('stream', onStreaming)
            wsRef.current.on('close', onClose)
            wsRef.current.on('error', onError)
            wsRef.current.on('connect_error', onConnectError)
        }
    }, [onClose, onOauthMessage, onOpen])

    useEffect(() => {
        if (streamSent && wsRef.current) {
            wsRef.current.emit('stream', streamSent)
        }
    }, [streamSent])

    useEffect(() => {
        if (wsRef.current) {
            if (live) {
                wsRef.current.emit('subscribe', { room: 'live' })
            } else {
                wsRef.current.emit('unsubscribe', { room: 'live' })
            }
        }
    }, [live])

    useEffect(() => {
        if (!isOnline && ws.opened) {
            dispatch(wsClosed())
        }
        if (isOnline && (!ws.opened && wsRef.current)) {
            dispatch(wsOpened(wsRef.current.id))
        }
    }, [isOnline, ws.opened, wsRef.current])

    return [wsRef.current, initWebsocket]
}
export default useWebsocket