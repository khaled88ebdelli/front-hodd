import React from 'react'
import LayoutIndex from '../../components/Layout'
import {Profile} from '../../features/profile/Profile'

function ProfilePage(props) {
    return (
        <LayoutIndex>
            <Profile {...props}/>
        </LayoutIndex>
    )
}

export default ProfilePage