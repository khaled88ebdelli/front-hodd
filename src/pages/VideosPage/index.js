import React from 'react'
import LayoutIndex from '../../components/Layout'
import { Videos } from '../../features/videos/Videos'

function VideosPage(props) {
    return (
        <LayoutIndex>
            <Videos {...props}/>
        </LayoutIndex>
    )
}

export default VideosPage