import React from 'react'
import { Result, Button } from 'antd'
import { A } from 'hookrouter'

function NotFoundPage() {
    return (
        <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={<Button type="primary"><A href="/">Back Home</A></Button>}
  />
    )
}

export default NotFoundPage
