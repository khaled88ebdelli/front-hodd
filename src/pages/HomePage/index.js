import React from 'react'
import LayoutIndex from '../../components/Layout'

function HomePage() {
    return (
        <LayoutIndex>
            <h1>Home Page</h1>
        </LayoutIndex>
    )
}

export default HomePage
