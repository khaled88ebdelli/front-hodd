import React from 'react'
import { Result } from 'antd'
import { SmileOutlined } from '@ant-design/icons';

function NotInternetPage() {
    return (
        <Result
            title="Check your internet connection"
            extra={<SmileOutlined rotate={180} twoToneColor="#FF0000" />}
        />
    )
}

export default NotInternetPage
