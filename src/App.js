/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useRoutes } from 'hookrouter';
import { publicRoutes, privateRoutes } from "./helpers/router.config";
import useInternet from './hooks/useInernet'
import useWebsocket from "./hooks/useWebsocket";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { selectLoginState } from "./features/login/LoginSlice";
import NotFoundPage from "./pages/NotFoudPage";
import { Login } from "./features/login/Login";
import NotInternetPage from "./pages/NotInternetPage";
import Chatbox from "./features/chatbox/Chatbox";
import { selectChatbox, closeChatbox } from "./features/chatbox/chatboxSlice";

const App = _props => {
  const dispatch = useDispatch();
  const { isOnline } = useInternet()
  const [,initWebsocket] = useWebsocket({ isOnline })
  const { isLogged } = useSelector(selectLoginState)
  const { visible } = useSelector(selectChatbox)
  const publicroutes = useRoutes(publicRoutes);
  const privateroutes = useRoutes(privateRoutes);
  const onCloseChatbox = () => dispatch(closeChatbox())

  useEffect(() => {
    initWebsocket()
  }, [])
  return (<>{isOnline &&
    <>
    <Chatbox visible={visible} onClose={onCloseChatbox} />
      {isLogged && (privateroutes || <NotFoundPage />)}
      {!isLogged && (publicroutes || <Login />)}
    </>
  } {!isOnline && <div style={{ width: '30%', margin: '0 auto' }}>
    <h1><NotInternetPage /></h1>
  </div>}</>)
};
export default App;