import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Form, Button, Spin, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { fetchProfileAsync, selectProfile, upateProfileAsync } from './ProfileSlice'
import UserInfo from '../../components/UserInfo';
import ProfileUpdate from './ProfileUpdate';

export const Profile = () => {
  const [visible, setVisible] = useState(false)
  const {loading, ...profile} = useSelector(selectProfile)
  const dispatch = useDispatch()
  const [form] = Form.useForm();

  const showDrawer = () => {
    setVisible(true)
  };
  const onSubmit = () => {
    dispatch(upateProfileAsync(form.getFieldsValue()))
    setVisible(false)
  };
  const onClose = () => {
    setVisible(false)
  };


  useEffect(() => {
    dispatch(fetchProfileAsync())
  }, [dispatch])

  return (
    <div>
      <Spin spinning={loading}>
      <Button
        type="primary"
        onClick={showDrawer}>
        <PlusOutlined /> Update profile
        </Button>
        <Divider >Profile informations</Divider>
        <UserInfo {...profile} />
      </Spin>
      <ProfileUpdate
        form={form}
        visible={visible}
        onClose={onClose}
        onSubmit={onSubmit}
       />
    </div>
  );
}