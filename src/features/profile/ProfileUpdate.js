import React from 'react'
import moment from 'moment';
import { Drawer, Form, Button, Col, Row, Input, DatePicker } from 'antd';
import { useSelector } from 'react-redux';
import { selectProfile } from './ProfileSlice';

function ProfileUpdate({onClose, onSubmit, visible, form}) {
    const dateFormat = 'DD/MM/YYYY';
    const profile = useSelector(selectProfile)
    const defaultBirthdate = profile?.birthdate ? moment(new Date(profile.birthdate), dateFormat):
    moment(new Date(), dateFormat)
    const initialValues = {
     firstname: profile.firstname,
     lastname: profile.lastname,
     email: profile.email,
     birthdate: defaultBirthdate
    }
    return (
        <Drawer
        title="Create a new account"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <Form.Item style={{ textAlign: 'right' }}>
            <Button onClick={onClose} style={{ marginRight: 8 }}> Cancel </Button>
            <Button onClick={onSubmit} type="primary"> Submit </Button>
          </Form.Item>
        }
      >
        <Form form={form}
        initialValues={initialValues}
        layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="firstname"
                label="First Name"
                rules={[{ required: true, message: 'Please enter user firstname' }]}>
                <Input placeholder="Please enter firstname" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="lastname"
                label="Last Name"
                rules={[{ required: true, message: 'Please enter lastname' }]}
              >
                <Input style={{ width: '100%' }} placeholder="Please enter lastname" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="email"
                label="Email"
                rules={[{ required: true, message: 'Please select email' }]}
              >
                <Input
                  style={{ width: '100%' }}
                  disabled
                  type="email"
                  placeholder="Please enter email"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="birthdate"
                label="Birth date"
                rules={[{ required: true, message: 'Please choose your birthdate' }]}
              >
                <DatePicker
                  style={{ width: '100%' }}
                  format={dateFormat} />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
   
    )
}

export default ProfileUpdate
