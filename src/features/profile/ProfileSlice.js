import { createSlice } from '@reduxjs/toolkit';
import Api from '../../helpers/Api';

export const slice = createSlice({
  name: 'profile',
  initialState: {
    firstname: null,
    lastname: null,
    email: null,
    picture: null,
    provider: null,
    birthdate: null,
    loading: false,
    error: null
  },
  reducers: {
    fetchProfile: state => {
      state.loading = true;
    },
    fetchProfileSuccess: (state, action) => {
      state.firstname = action.payload.firstname;
      state.lastname = action.payload.lastname;
      state.email = action.payload.email;
      state.picture = action.payload.picture;
      state.provider = action.payload.provider;
      state.birthdate = action.payload.birthdate;
      state.loading = false;
    },
    fetchProfileFailed: (state, action) => {
      state.firtname = null;
      state.lastname = null;
      state.email = null;
      state.picture = null;
      state.provider = null;
      state.birthdate = null;
      state.loading = false;
      state.error = action.payload;
    },
    updateProfileSuccess: (state, action) => {
      state.firstname = action.payload.firstname;
      state.lastname = action.payload.lastname;
      state.email = action.payload.email;
      state.picture = action.payload.picture;
      state.provider = action.payload.provider;
      state.birthdate = action.payload.birthdate;
      state.loading = false;
    },
  },
});

export const { fetchProfile, fetchProfileSuccess, fetchProfileFailed, updateProfileSuccess } = slice.actions;

export const fetchProfileAsync = () => dispatch => {
  dispatch(fetchProfile());
  return Api.get('/users/current')
  .then(response => {
    if(response.statusText === 'OK') {
      dispatch(fetchProfileSuccess(response.data))
    }
    console.log('response', response);
  })
  .catch(error => {
    console.log(error);
    dispatch(fetchProfileFailed(error.message))
  })
};

export const upateProfileAsync = (user) => dispatch => {
  dispatch(fetchProfile());
  return Api.put('/users', user)
  .then(response => {
    if(response.statusText === 'OK') {
      dispatch(updateProfileSuccess(response.data))
    }
  })
  .catch(error => {
    console.log(error);
    dispatch(fetchProfileFailed(error.message))
  })
};

export const selectProfile = state => state.profile;

export default slice.reducer;
