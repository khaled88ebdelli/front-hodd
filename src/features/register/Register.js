/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Form, Input, Button, Row, Col, Spin, Checkbox, Divider, message, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

import { RegisterAsync, selectRegisterState } from './RegisterSlice';
import SocialAuth from '../../components/SocialAuth';
import { A, navigate } from 'hookrouter';
import { useEffect } from 'react';

export function Register() {
  const registerState = useSelector(selectRegisterState);
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    if (values?.email !== '' && values?.password !== '') {
      dispatch(RegisterAsync(values))
      console.log('values:', values);
    }
  };
  useEffect(() => {
    if (registerState.error) {
      registerState.error.forEach(err => {
        message.error(`${err.value} ${err.msg}`)
      })
    }
  }, [registerState.error])

  useEffect(() => {
    if (registerState.registred) {
      notification.success('Registred Successfuly')
      navigate('/login', true)
    }
  }, [registerState.registred])
  // const onFinishFailed = (errorInfo) => {
  //   console.log('Failed:', errorInfo);
  // };

  return (
    <Row justify="center" align="middle" style={{ paddingTop: '10%' }}>
      <Col md={{ span: 8 }} lg={{ span: 6 }} xs={{ span: 20 }} sm={{ span: 12 }}>
        <Col md={{ span: 8 }} lg={{ span: 8 }} xs={{ span: 20, push: 10 }} sm={{ span: 8 }}>
          {registerState.loading && <Spin size="large" />}
        </Col>
        <SocialAuth auth="Register" />
        <Divider>Or</Divider>
        <div className="login-form">
          <Form
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
              residence: ['zhejiang', 'hangzhou', 'xihu'],
              prefix: '86',
            }}
            scrollToFirstError
          >
            <Form.Item name="email"
              rules={[
                { type: 'email', message: 'The input is not valid E-mail!' },
                { required: true, message: 'Please input your E-mail!' },
              ]}>
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
            </Form.Item>

            <Form.Item name="password" hasFeedback
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}>
              <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Password" />
            </Form.Item>

            <Form.Item
              name="confirm"
              dependencies={['password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                ({ getFieldValue }) => ({
                  validator(_rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject('The two passwords that you entered do not match!');
                  },
                }),
              ]}
            >
              <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Confirm Password" />
            </Form.Item>

            <Form.Item
              name="agreement"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value ? Promise.resolve() : Promise.reject('Should accept agreement'),
                },
              ]}
            >
              <Checkbox>
                I have read the <a href="">agreement</a>
              </Checkbox>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Register
        </Button>
        Have an account? <A href="/login">Login</A>
            </Form.Item>
          </Form>
        </div>
      </Col>
    </Row>

  );
}
