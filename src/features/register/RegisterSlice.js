import { createSlice } from '@reduxjs/toolkit';
import Api from '../../helpers/Api';





const initialState = {
  registred: false,
  loading: false,
  error: null,
};

export const slice = createSlice({
  name: 'register',
  initialState,
  reducers: {
    register: state => {
      localStorage.removeItem('token'); 
      state.loading = true;
    },
    registerSuccess: state => {
      state.loading = false;  
      state.isLogged = true;
      state.registred = true;
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    registerFailed: (state, action) => {
      state.error = action.payload.response.data.errors;
      localStorage.removeItem('token');
      state.loading = false;
      state.registred = false;
    },
  },
});

export const { register, registerSuccess, registerFailed } = slice.actions;

export const RegisterAsync = (user) => (dispatch) => {
  dispatch(register());
  return Api.post('/users/register', user)
  .then(response => dispatch(registerSuccess(response)))
  .catch(error => dispatch(registerFailed(error)))
};

export const selectRegisterState = state => state.register;

export default slice.reducer;
