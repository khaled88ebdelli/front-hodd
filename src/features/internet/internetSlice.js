import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
    name: 'internet',
    initialState: {
        isOnline: true,
        ws: {
            id: null,
            connecting: false,
            opened: false,
            message: {
                event: '',
                payload: ''
            }
        },
    },
    reducers: {
        internetOnline: state => {
            state.isOnline = true
        },
        internetOffline: state => {
            state.isOnline = false
        },
        wsConnecting: state => {
            state.ws.connecting = true
        },
        wsOpened: (state, action) => {
            state.ws.connecting = false
            state.ws.opened = true
            state.ws.id = action.payload
        },
        wsClosed: state => {
            state.ws.connecting = false
            state.ws.opened = false
            state.ws.id = null
            state.ws.message = {
                event: '',
                payload: ''
            }
        },
        wsOnMessage: (state, action) => {
            state.ws.message = action.payload
        },
        clearMessage: state => {
            state.ws.message = {
                event: '',
                payload: ''
            }
        }
    },
});

export const {
    internetOnline,
    internetOffline,
    wsConnecting,
    wsOpened,
    wsClosed,
    wsOnMessage,
    clearMessage
} = slice.actions;

export const selectInternet = state => state.internet;

export default slice.reducer;