import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Drawer, Col, Row, Input, List, Avatar, Button, Spin } from 'antd';
import { isMobile } from 'react-device-detect';
import moment from 'moment';
import InfiniteScroll from 'react-infinite-scroller';
import { selectChatbox, sendMessageAsync, fetchMessageAsync } from './chatboxSlice';
import { EnterOutlined } from '@ant-design/icons';
// import styles from './Chatbox.module.css';
function Chatbox({ onClose, visible }) {
    const { messages, loading, hasMore } = useSelector(selectChatbox)
    const [message, setMessage] = useState('');
    const [page, setPage] = useState(0)
    let drawerBody = document.getElementsByClassName('ant-drawer-body')[0]
    const dispatch = useDispatch()
    const sendHandler = (e) => {
        e.preventDefault()
        if (message !== '') {
            dispatch(sendMessageAsync(message))
            setMessage(null)
            drawerBody = document.getElementsByClassName('ant-drawer-body')[0]
        }

    }
    const onChangeMessage = (e) => {
        setMessage(e.target.value)
    }
    const handleInfiniteOnLoad = () => {
        if (!hasMore) return
        dispatch(fetchMessageAsync(page))
        setPage(p => p += 10)
    }
    useEffect(() => {
        if (drawerBody) {
            setTimeout(() => drawerBody.scrollTo(0, drawerBody.scrollHeight))
        }
    }, [messages, drawerBody])
    useEffect(() => {
        if (visible) {
           setTimeout(() => dispatch(fetchMessageAsync()), 1000)
        }
    }, [dispatch, visible])
    return (
        <Drawer
            title="ChatBox"
            width={isMobile ? '90%' : 400}
            onClose={onClose}
            visible={visible}
            bodyStyle={{ paddingBottom: 80 }}
            footer={
                <Row gutter={16} style={{paddingRight: '10px'}}>
                    <Col span={20}>
                        <Input.TextArea
                            onChange={onChangeMessage}
                            onPressEnter={sendHandler}
                            value={message}
                            rows={1}
                            placeholder="send message here" />
                    </Col>
                    <Col span={4}>
                        <Button
                            type="primary"
                            shape="round"
                            icon={<EnterOutlined />}
                            onClick={sendHandler} />
                    </Col>
                </Row>
            }
        >
            <InfiniteScroll
                initialLoad={false}
                isReverse={true}
                pageStart={0}
                loadMore={handleInfiniteOnLoad}
                hasMore={!loading && hasMore}
                useWindow={false}
            >  {loading && <Spin style={{position: 'absolute', left: '45%'}} />}
                <List
                    itemLayout="vertical"
                    size="small"
                    dataSource={messages}
                    renderItem={item => (
                        <List.Item key={item.id}>
                            <List.Item.Meta
                                avatar={<Avatar src={item.user.picture} />}
                                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                title={<a href="">{item.user.firstname} {item.user.lastname}</a>}
                                description={moment(item.createdAt).format('LTS') + ' ' + moment(item.createdAt).fromNow()}
                            />
                            {item.message}
                        </List.Item>
                    )}
                />
            </InfiniteScroll>
        </Drawer>

    )
}

export default Chatbox
