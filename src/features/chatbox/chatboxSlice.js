import { createSlice } from '@reduxjs/toolkit';
import Api from '../../helpers/Api';

export const slice = createSlice({
    name: 'chatbox',
    initialState: {
        messages: [],
        visible: false,
        newMessages: 0,
        joined: [],
        loading: false,
        hasMore: true
    },
    reducers: {
        showChatbox: state => {
            state.visible = true
        },
        closeChatbox: state => {
            state.visible = false
        },
        sendMessage: state => {
            if(state.newMessages !== 0) {
                state.newMessages -= 1
            }
        },
        receiveMessage: (state, action) => {
            state.messages.push(action.payload)
            state.newMessages += 1
        },
        fetchMessages: state => {
            state.loading = true
            state.newMessages = 0
        },
        fetchMessagesSuccess: (state, action) => {
            state.messages = [...action.payload, ...state.messages]
            state.newMessages = 0
            state.loading = false
            if(action.payload.length < 10) {
                state.hasMore = false
            }
        },
        joinChatBox: (state, action) => {
            state.joined.push(action.payload.user)
        }
    },
});

export const {
    showChatbox,
    closeChatbox,
    sendMessage,
    fetchMessages,
    fetchMessagesSuccess,
    joinChatBox,
    clearChatBox,
    receiveMessage
} = slice.actions;

export const selectChatbox = state => state.chatbox;

export const sendMessageAsync = (msg) => (dispatch) => {
    return Api.post('/chatbox', { message: msg })
        .then(_response => dispatch(sendMessage()))
        .catch(error => console.log(error));
};

export const fetchMessageAsync = (page = 0) => (dispatch) => {
    dispatch(fetchMessages())
    return Api.get(`/chatbox`,{params: {page: page}})
        .then(response => dispatch(fetchMessagesSuccess(response.data)))
        .catch(error => console.log(error));
};
export default slice.reducer;