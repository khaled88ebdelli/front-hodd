import React, { useState, useEffect } from 'react'
import VideoCard from '../../components/shared/VideoCard/index';
import { Row, Col, Button, Divider, Spin } from 'antd';
import VideoAdd from './VideoAdd';
import { useSelector, useDispatch } from 'react-redux';
import { selectVideos, fetchVideosAsync, addVideoAsync, deleteVideoAsync } from './VideosSlice';
import VideoModal from './VideoModal';
import { PlayCircleOutlined, VideoCameraAddOutlined } from '@ant-design/icons';


export const Videos = () => {
  const { videos, loading, loadingUpload, loadingDelete, mediaBlobUrl } = useSelector(selectVideos)
  const [videoAddVisible, setVideoAddVisible] = useState(false)
  const [modalVisible, setModalVisible] = useState(false)
  const [selectedVideo, setSelectedVideo] = useState()
  const dispatch = useDispatch()

  useEffect(() => {
    if (!loadingUpload && !loadingDelete) {
      setVideoAddVisible(false)
      setModalVisible(false)
      dispatch(fetchVideosAsync())
    }
  }, [dispatch, loadingDelete, loadingUpload])

  const showDrawer = () => {
    setVideoAddVisible(true)
  };

  const onSubmit = (title, description) => {
    if (mediaBlobUrl) {
      dispatch(addVideoAsync(mediaBlobUrl, title, description))
      setVideoAddVisible(false)
    }
  };
  const onOpenVideoMdal = (video) => {
    setModalVisible(true)
    setSelectedVideo(video)
  }
  const onCloseVideoMdal = () => {
    setModalVisible(false)
    setSelectedVideo(null)
  }
  function confirmDelete(e) {
    console.log(e);
    dispatch(deleteVideoAsync(selectedVideo.filename))
    setSelectedVideo(null)
  }

  function cancelDelete(e) {
    console.log(e);
    setModalVisible(true)
  }

  const onCloseVideoAdd = () => {
    setVideoAddVisible(false)
  };

  return (
    <div>
      <Button onClick={showDrawer}
        icon={mediaBlobUrl? <PlayCircleOutlined /> : <VideoCameraAddOutlined />}
        loading={loadingUpload}
        style={{ width: loadingUpload ? '200px' : 'auto' }}> {loadingUpload ? 'Uploading' : mediaBlobUrl ? 'Play Recorded' : 'Start Recording'} video</Button>
      <Divider />
      <Row gutter={0} >
        {loading && <Spin style={{ margin: '0 auto' }} />}
        {!loading && videos &&
          videos.map((video, index) => (
            <Col key={video._id} xs={24} sm={8} lg={6} span={12}>
              <VideoCard
                openVideoModal={() => onOpenVideoMdal(video)}
                {...video} loading={loading} />
            </Col>
          ))
        }
      </Row>
      <VideoAdd visible={videoAddVisible} onClose={onCloseVideoAdd} onSubmit={onSubmit} />
      <VideoModal
        video={selectedVideo}
        visible={modalVisible}
        onClose={onCloseVideoMdal}
        confirm={confirmDelete}
        cancel={cancelDelete}
      />
    </div>
  );
}