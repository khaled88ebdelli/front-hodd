import { createSlice } from '@reduxjs/toolkit';
import Api from '../../helpers/Api';
import { wrap } from "comlink";
import { extractFramesFromVideo } from '../../helpers/extractFramesFromVideo';
import { message } from 'antd';

const fetchBlob = wrap(new Worker('././../../videos.worker', { type: 'module' }));

export const slice = createSlice({
  name: 'videos',
  initialState: {
    videos: [],
    loading: false,
    recording: false,
    mediaBlobUrl: null,
    loadingUpload: false,
    loadingDelete: false,
    live: false,
    streamSent: null,
    streamReceived: null,
  },
  reducers: {
    fetchVideos: state => {
      state.loading = true;
    },
    fetchVideosSuccess: (state, action) => {
      state.videos = action.payload
      state.loading = false;
    },
    fetchVideosFailed: (state, action) => {
      state.videos = []
      state.loading = false;
      state.error = action.payload;
    },
    loadMediaBlobUrl: (state, action) => {
      state.mediaBlobUrl = action.payload
      state.recording = false
    },
    recordingStart: state => {
      state.recording = true
    },
    recordingStop: state => {
      state.recording = false
    },
    addVideo: state => {
      state.loadingUpload = true
    },
    addVideoSuccess: state => {
      state.loadingUpload = false
      state.mediaBlobUrl = null
    },
    addVideoFailed: (state, action) => {
      state.loadingUpload = false
      state.error = action.payload
    },
    deleteVideo: state => {
      state.loadingDelete = true
    },
    deleteVideoSuccess: state => {
      state.loadingDelete = false
    },
    deleteVideoFailed: (state, action) => {
      state.loadingDelete = false
      state.error = action.payload
    },
    newStreamSent: (state, action) => {
      state.streamSent = action.payload
    },
    newStreamReceived: (state, action) => {
      state.streamReceived = action.payload
    },
    joinLive: state => {
      state.live = true
    },
    leaveLive: state => {
      state.live = false
    }
  },
});

export const {
  fetchVideos,
  fetchVideosSuccess,
  fetchVideosFailed,
  loadMediaBlobUrl,
  recordingStart,
  recordingStop,
  addVideo,
  addVideoSuccess,
  addVideoFailed,
  deleteVideo,
  deleteVideoSuccess,
  deleteVideoFailed,
  newStreamSent,
  newStreamReceived,
  joinLive,
  leaveLive
} = slice.actions;

export const fetchVideosAsync = () => async (dispatch) => {
  dispatch(fetchVideos());
  const response = await Api.get('/videos')
  if(response.data) {
    dispatch(fetchVideosSuccess(response.data))
  } else {
    dispatch(fetchVideosFailed(response))
  }
};

export const deleteVideoAsync = (videoId) => async (dispatch) => {
  dispatch(deleteVideo());
  await new Promise(r => setTimeout(r, 5000))
  const response = await Api.delete('/videos/'+videoId)
  if(response.data) {
    dispatch(deleteVideoSuccess())
    message.success('Click on Yes')
  } else {
    dispatch(deleteVideoFailed(response))
  }
};


export const addVideoAsync = (mediaBlobUrl, title, description) => async (dispatch) => {
  dispatch(addVideo())
  const URL = window.URL || window.webkitURL;
  const [base64ImgDataOriginal, base64ImgDataResized] = await extractFramesFromVideo(mediaBlobUrl)
  const blob = await fetchBlob(mediaBlobUrl)
  const blobPoster = await fetchBlob(base64ImgDataOriginal);
  const blobThumbnail = await fetchBlob(base64ImgDataResized);
  URL.revokeObjectURL(mediaBlobUrl)
  URL.revokeObjectURL(base64ImgDataOriginal)
  URL.revokeObjectURL(base64ImgDataResized)
  const videoFile = new File([blob], "video", { type: 'video/webm', duration: 10 });
  const posterFile = new File([blobPoster], "poster", { type: 'image/png' });
  const thumbnailFile = new File([blobThumbnail], "thumbnail", { type: 'image/png' });
  const data = new FormData();

  data.append('video', videoFile)
  data.append('poster', posterFile)
  data.append('thumbnail', thumbnailFile)
  data.append('title', title)
  data.append('description', description)

  return Api.post('/videos', data)
    .then(response => {
      if (response.statusText === 'OK') {
        dispatch(addVideoSuccess())
      }
    })
    .catch(error => {
      console.log(error);
      dispatch(addVideoFailed(error.message))
    })
};


export const selectVideos = state => state.videos;

export default slice.reducer;
