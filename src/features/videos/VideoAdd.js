import React from 'react'
import { Drawer, Button, Modal, Form, Input, Radio } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { isMobile } from 'react-device-detect';
import { selectVideos, loadMediaBlobUrl, newStreamSent } from './VideosSlice';
import MediaRecorder from '../../components/Recorder';


function VideoAdd({ onClose, onSubmit, visible }) {
  const { mediaBlobUrl, loadingUpload } = useSelector(selectVideos)
  const [modal] = Modal.useModal();
  const dispatch = useDispatch()
  const [form] = Form.useForm();
  const config = {
    title: 'Use Hook!',
    content: (
      <div>
        <Form
          form={form}
          layout="vertical"
          name="form_in_modal"
          initialValues={{ modifier: 'public' }}
        >
          <Form.Item
            name="title"
            label="Title"
            rules={[{ required: true, message: 'Please input the title of collection!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input type="textarea" />
          </Form.Item>
          <Form.Item name="modifier" className="collection-create-form_last-form-item">
            <Radio.Group>
              <Radio value="public">Public</Radio>
              <Radio value="private">Private</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </div>
    ),
  };
  const cancelHandler = () => {
    onClose()
    dispatch(loadMediaBlobUrl(null))
    dispatch(newStreamSent(null))
  }
  return (
    <Drawer
      title="Create a new video"
      width={isMobile ? '100%' : 720}
      onClose={onClose}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
      footer={
        <div style={{ textAlign: 'right' }}>
          <Button onClick={cancelHandler} style={{ marginRight: 8 }}>
            Cancel</Button>
          <Button disabled={!mediaBlobUrl} loading={loadingUpload} onClick={() => modal.info(config)} type="primary">
            Submit</Button>
        </div>
      }
    >
      {visible && <MediaRecorder visible={visible} onSubmit={onSubmit} />}
    </Drawer>

  )
}

export default VideoAdd
