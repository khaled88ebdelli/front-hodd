import React from 'react'
import { Modal, Popconfirm, Card } from 'antd'
import { DeleteOutlined, EditOutlined, EllipsisOutlined } from '@ant-design/icons'
import { useSelector } from 'react-redux'
import { selectVideos } from './VideosSlice'
import LoadingBar from '../../components/LoadingBar'
import Meta from 'antd/lib/card/Meta'
import Description from '../../components/shared/VideoDescription'

function VideoModal({ video, visible, onClose, confirm, cancel }) {
  const { loadingDelete } = useSelector(selectVideos)
  return (
    <Modal
      title={video?.title}
      centered
      visible={visible}
      onOk={onClose}
      maskClosable
      onCancel={onClose}
      bodyStyle={{ padding: 0 }}>
      {video && !loadingDelete &&
        <Card
          cover={
            <video
              style={{ width: "100%", maxHeight: "300px" }}
              src={`${process.env.REACT_APP_API_URL}/videos/${video.filename}`}
              autoPlay controls
              type="video/webm">
            </video>
          }
          actions={[
            <Popconfirm
              title="Are you sure to delete this video?"
              onConfirm={confirm}
              onCancel={cancel}
              okText="Delete Video"
              cancelText="Cancel"
            > <DeleteOutlined>Delete</DeleteOutlined> </Popconfirm>,
            <EditOutlined key="edit" />,
            <EllipsisOutlined key="ellipsis" />,
          ]}>
          <Meta title="Card title" description={<Description user={video.user} createdAt={video.createdAt} />} />
        </Card>}
      {loadingDelete && <LoadingBar />}
    </Modal>

  )
}

export default VideoModal
