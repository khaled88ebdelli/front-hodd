/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Form, Input, Button, Checkbox, Row, Col, Spin, Divider } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { LogInAsync, selectLoginState } from './LoginSlice';
import SocialAuth from '../../components/SocialAuth';
import { A, navigate } from 'hookrouter';
import { useEffect } from 'react';


export function Login() {
  const { isLogged, loading } = useSelector(selectLoginState);
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    if (values?.email !== '' && values?.password !== '') {
      dispatch(LogInAsync(values))
      navigate('/', true)
    }
  };
  useEffect(() => {
    if (isLogged) {
      navigate('/', true)
    }
  }, [isLogged])

  return (
    <Row justify="center" align="middle" style={{ paddingTop: '10%' }}>
      <Col md={{ span: 8 }} lg={{ span: 6 }} xs={{ span: 20 }} sm={{ span: 12 }}>
        <Col md={{ span: 8 }} lg={{ span: 8 }} xs={{ span: 20, push: 10 }} sm={{ span: 8 }}>
          {loading && <Spin size="large" />}
        </Col>
        <div className="login-form">
          <Form
            form={form}
            name="normal_login"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Form.Item
              name="email"
              rules={[{ required: true, message: 'Please input your Username!' }]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Please input your Password!' }]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="">
                Forgot password
              </a>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
                Or <A href="/register"> Register Now! </A>
            </Form.Item>
          </Form>
          <Divider>OR</Divider>
          <SocialAuth auth="Connect" />
        </div>
      </Col>
    </Row>

  );
}
