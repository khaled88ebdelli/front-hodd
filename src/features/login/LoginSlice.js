import { createSlice } from '@reduxjs/toolkit';
import { decodeToken } from '../../helpers/JwtHelper';
import Api from '../../helpers/Api';


let token = localStorage.getItem('token') === 'undefined' ? null :  localStorage.getItem('token')
let decodedToken = decodeToken(token);



const initialState = {
  email: decodedToken.email,
  firstname: decodedToken.firstname,
  lastname: decodedToken.lastname,
  provider: decodedToken.provider,
  picture: decodedToken.picture,
  token: token,
  loading: false,
  isLogged: !!token,
  error: null,
};

export const slice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    login: state => {
      localStorage.removeItem('token'); 
      state.loading = true;
    },
    loginSuccess: (state, action) => {
      state.picture = action.payload?.picture || '';
      state.provider = action.payload?.provider || '';
      state.firstname = action.payload?.firstname || '';
      state.lastname = action.payload?.lastname || '';
      state.email = action.payload.email;
      state.token = action.payload.token;
      localStorage.setItem('token', action.payload.token); 
      state.loading = false;  
      state.isLogged = true;
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    loginFailed: (state, action) => {
      state.error = action.payload;
      localStorage.clear()
      state.provider = '';
      state.email = '';
      state.firstname = '';
      state.lastname = '';
      state.picture = '';
      state.token = '';
      state.loading = false;
      state.isLogged = false;
    },
    signout: state => {
      localStorage.clear() 
      state.provider = '';
      state.email = '';
      state.firstname = '';
      state.lastname = '';
      state.picture = '';
      state.token = '';
      state.loading = false;  
      state.isLogged = false;
    },
    popupClose: state => {
      state.loading = false;  
    },
  },
});

export const { login, loginSuccess, loginFailed, signout, popupClose } = slice.actions;

export const LogInAsync = (user) => (dispatch) => {
  dispatch(login());
  return Api.post('/users/login', user)
  .then(response => dispatch(loginSuccess(response.data)))
  .catch(error => dispatch(loginFailed(error)))
};


export const selectLoginState = state => state.login;

export default slice.reducer;
